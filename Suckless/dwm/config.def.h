/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 6;       	/* border pixel of windows */
static const unsigned int gappx     = 10;     	/* gaps between windows */
static const unsigned int snap      = 1 << 2;       	/* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int vertpad            = 6;       /* vertical padding of bar */
static const int sidepad            = 1 << 9;       /* horizontal padding of bar */
static const char *fonts[]          = { "Fira Code Medium:style=Medium:size=14"};
static const char dmenufont[]       = "FiraMono Nerd Font Mono:size=14";
static const char bg_normal[]       = "#282828";
static const char bg_selected[]     = "#98971a";
static const char fg_normal[]       = "#d5c4a1";
static const char fg_selected[]     = "#ebdbb2";
static const char border_normal[]   = "#3c3836";
static const char border_selected[] = "#d65d0e";
static const char *colors[][3]      = {
	/*               fg             bg              border   */
	[SchemeNorm] = { fg_normal,     bg_normal,      border_normal },
	[SchemeSel]  = { fg_selected,   bg_selected,    border_selected },
};

/* tagging */
static const char *tags[]       = { "", "", "", "", "", "", "", "", "" };
static const char *dualtags[]   = { "", "" };  /* 1st - active, 2nd - empty */
static const int useTags = 0;   /* 1 - tags, 0 - circles */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       1 << 8,       1,           -1 },
	{ "Firefox",  NULL,       NULL,       0,            0,           -1 },
};

/* layout(s) */
static const float mfact     	= 0.5; 	/* factor of master area size [0.05..0.95] */
static const int nmaster     	= 1;    /* number of clients in master area */
static const int resizehints 	= 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; 	/* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]",      tile },    /* first entry is default */
	{ "[]",      NULL },
	{ "[]",      monocle},    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]   = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, NULL };
static const char *termcmd[]    = { "st", NULL };
static const char *wezterm[]    = { "wezterm", NULL };
static const char *browsercmd[] = { "firefox", NULL };
static const char *telegram[]   = { "telegram-desktop", NULL };
static const char *upvol[]      = { "/home/george/.local/bin/volumeChange", "+10%", NULL };
static const char *downvol[]    = { "/home/george/.local/bin/volumeChange", "-10%", NULL };
static const char *mutevol[]    = { "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL };
static const char *brightness[] = {"/home/george/.local/bin/brightnessChange", NULL};   // ACPID has a built-in scrip that changes the brightness. Thhis only throws a notification
static const char *prtScFull[]  = { "flameshot", "full", "-c", "-p", "/W/Media", NULL};
static const char *prtScGUI[]   = { "flameshot", "gui", NULL};
static const char *dmenuemoji[] = { "dmenuunicode", NULL };
static const char *redshift_toggle[] = { "redshift-toggle", NULL };
static const char *calendar[] = { "/home/george/.local/bin/calendunst", NULL };
static const char *dmenu_inlyne[] = { "/home/george/.local/bin/dmenu-inlyne", NULL };
//static const char *dmenukill[]  = { "/home/george/.local/bin/dmenukill", NULL };
//static const char *clipmenu[]   = { "/usr/bin/clipmenu", NULL};
//static const char *spawnSlock[] = { "/usr/local/bin/slock", NULL};

#include <X11/XF86keysym.h>	//https://github.com/freedesktop/xorg-x11proto/blob/master/XF86keysym.h

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                   XK_Return,      spawn,          {.v = wezterm } },
	{ MODKEY,                   XK_F1,          spawn,          {.v = termcmd } },
	{ MODKEY,                   XK_F2,          spawn,          {.v = browsercmd } },
	{ MODKEY,                   XK_r,           spawn,          {.v = dmenucmd } },
	{ MODKEY,                   XK_F4,          spawn,          {.v = telegram } },
	//{ MODKEY|ShiftMask,         XK_c,           spawn,          {.v = clipmenu } },
    { MODKEY|ShiftMask,         XK_e,           spawn,          {.v = dmenuemoji } },
    { MODKEY|ShiftMask,         XK_i,           spawn,          {.v = dmenu_inlyne } },
	//{ MODKEY|ShiftMask,         XK_k,           spawn,          {.v = dmenukill } },
	{ MODKEY,                   XK_b,           togglebar,      {0} },
	{ MODKEY,                   XK_a,           focusstack,     {.i = +1 } },
	{ MODKEY,                   XK_d,           focusstack,     {.i = -1 } },
	{ MODKEY,                   XK_i,           incnmaster,     {.i = +1 } },
	{ MODKEY,                   XK_o,           incnmaster,     {.i = -1 } },
	{ MODKEY,                   XK_q,           setmfact,       {.f = -0.05} },
	{ MODKEY,                   XK_e,           setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,         XK_a,           movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,         XK_d,           movestack,      {.i = -1 } },
	{ MODKEY,                   XK_z,           setcfact,       {.f = +0.25} },
	{ MODKEY,                   XK_c,           setcfact,       {.f = -0.25} },
	{ MODKEY,                   XK_x,           setcfact,       {.f =  0.00} },
    { MODKEY,                   XK_Tab,         view,           {0} },
	{ MODKEY,                   XK_w,           killclient,     {0} },
	{ MODKEY,                   XK_t,           setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                   XK_f,           setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                   XK_m,           setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                   XK_space,       setlayout,      {0} },
	{ MODKEY|ShiftMask,         XK_space,       togglefloating, {0} },
	{ MODKEY,                   XK_0,           view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,         XK_0,           tag,            {.ui = ~0 } },
	{ MODKEY,                   XK_comma,       focusmon,       {.i = -1 } },
	{ MODKEY,                   XK_period,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,         XK_comma,       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,         XK_period,      tagmon,         {.i = +1 } },
	{ MODKEY,                   XK_Print,       spawn,          {.v = prtScGUI}},
    { 0,                        XK_Print,       spawn,          {.v = prtScFull}},
    //{ MODKEY,                   XK_BackSpace,   spawn,		   {.v = spawnSlock}},
	{ 0,            XF86XK_AudioLowerVolume,    spawn,          {.v = downvol } },
	{ 0,            XF86XK_AudioMute,           spawn,          {.v = mutevol } },
	{ 0,            XF86XK_AudioRaiseVolume,    spawn,          {.v = upvol   } },
	{ 0,            XF86XK_AudioPlay	,       spawn,          {.v = redshift_toggle   } },
	{ 0,            XF86XK_MonBrightnessUp,     spawn, 	        {.v = brightness}},
	{ 0,            XF86XK_MonBrightnessDown,   spawn,	        {.v = brightness}},
	{ MODKEY|ShiftMask,         XK_q,           quit,           {0} },
	TAGKEYS(                    XK_1,           0)
	TAGKEYS(                    XK_2,           1)
	TAGKEYS(                    XK_3,           2)
	TAGKEYS(                    XK_4,           3)
	TAGKEYS(                    XK_5,           4)
	TAGKEYS(                    XK_6,           5)
	TAGKEYS(                    XK_7,           6)
	TAGKEYS(                    XK_8,           7)
	TAGKEYS(                    XK_9,           8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkStatusText,        0,              Button1,        spawn,          {.v = calendar } },
};
