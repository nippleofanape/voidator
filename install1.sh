#!/usr/bin/bash

# Cargo install some programs
cargo_pkgs=(
    "yazi-fm"
    "taplo-cli"
    "presenterm"
)

# Services to activate
services_list=(
    "acpid"
    "agetty-tty1"
    "agetty-tty2"
    "agetty-tty3"
    "dbus"
    "dcron"
    "openntpd"
    "socklog-unix"
    "nanoklogd"
    "udevd"
    "NetworkManager"
)
for service in "${services_list[@]}"
do
    if [ ! -h /var/service/$service ]
    then
        doas ln -s /etc/sv/$service /var/service
    fi
done

mkdir -p ~/.{local/bin,cache,config}

# Link dot files from repo to home
dots=(
    ".Xresources"
    ".xinitrc"
    ".zprofile"
    ".zshenv"
    ".zshrc"
)
for file in "${dots[@]}"
do
    ln $file ~/
done

DIR=$(ls .config)
CWD=$(pwd)
for file in $DIR; do
    if [ -f .config/"$file" ]
    then
        ln .config/$file ~/.config
    else
        ln -s $CWD/.config/$file ~/.config
    fi
done

ln .local/bin/* ~/.local/bin

#Picom
git clone --depth=1 https://github.com/void-linux/void-packages Repos/void-packages
cd Repos/void-packages
./xbps-src binary-bootstrap
echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
git clone https://github.com/ibhagwan/picom-ibhagwan-template srcpkgs/picom-ibhagwan
./xbps-src pkg picom-ibhagwan
#TODO separate root script
doas xbps-install --repository=hostdir/binpkgs picom-ibhagwan 

# Touchpad gestures
git clone https://github.com/bulletmark/libinput-gestures.git ~/Repos
cd ~/Repos/libinput-gestures/
doas ./libinput-gestures-setup install

# NNN plugins
sh -c "$(curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs)"

# advcp/advmv for bar when copying
curl https://raw.githubusercontent.com/jarun/advcpmv/master/install.sh --create-dirs -o ./advcpmv/install.sh && (cd advcpmv && sh install.sh)
mv advcpmv/advcp ~/.local/bin && mv advcpmv/advmv ~/.local/bin

#zsh plugins and completions
mkdir -p ~/.local/share/{zplugs,zfunc}
rustup completions zsh > ~/.local/share/zfunc/_rustu
rustup completions zsh cargo > ~/.local/share/zfunc/_cargo
curl -L -o ~/.local/share/zfunc/_zig "https://raw.githubusercontent.com/ziglang/shell-completions/master/_zig"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.local/share/zplugs/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.local/share/zplugs/zsh-autosuggestions

# Git credentials and ssh key
ssh-keygen -t ed25519 -C "georgij.minarskij@proton.me"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_ed25519

git config --global user.name "George Mynarskyi"
git config --global user.email "georgij.minarskij@proton.me"

#TODO separate root script
make -C voidator/Suckless/dwm/ install
make -C voidator/Suckless/st/ install
make -C voidator/Suckless/dmenu/ install

#TODO separate root script
cat > /etc/cron.weekly/fstrim << EOF
#!/bin/sh

fstrim /
EOF
chmod +x /etc/cron.weekly/fstrim

# Maybe for future: substitute if exist and append if doesnt
# sed -i '/^GRUB_THEME=/{h;s/=.*/="/usr/share/grub/themes/Sekiro/theme.txt"/};${x;/^$/{s//GRUB_THEME="/usr/share/grub/themes/Sekiro/theme.txt"/;H};x}' /etc/default/grub
# sed -i '/^GRUB_DISABLE_OS_PROBER=/{h;s/=.*/=false/};${x;/^$/{s//GRUB_DISABLE_OS_PROBER=false/;H};x}' /etc/default/grub

# Installing rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup component add rust-analyzer

for pkg in "${cargo_pkgs[@]}"
do
    cargo install --locked "$pkg"
done

cargo install --git https://github.com/wgsl-analyzer/wgsl-analyzer wgsl_analyzer

# Zig
# Need zigup to install zig; zls, 
cd ~/Downloads
VERSION="0.13.0"
NAME="zig-linux-x86_64-$VERSION"
curl -L https://ziglang.org/builds/zig-linux-x86_64-$VERSION.tar.xz | tar -xJ
mv $NAME zig
ln -s ~/Downloads/zig/zig ~/.local/bin/
# download same version as zig compiler
curl -L -o ~/.local/bin/zls https://zig.pm/zls/downloads/x86_64-linux/bin/zls && chmod 755 zls

# Text files for nvim
curl -L https://github.com/valentjn/ltex-ls/releases/download/16.0.0/ltex-ls-16.0.0-linux-x64.tar.gz | tar -xz
ln -s ~/Downloads/ltex-ls-16.0.0/bin/ltex-ls ~/.local/bin/

#Run command as a user from root
su -c "git clone https://github.com/orhun/kmon.git ~/Repos/kmon" george
