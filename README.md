# voidator

Dotfiles for Void Linux and Suckless apps

![scr](./scr.png)

```
curl -L https://codeberg.org/nippleofanape/voidator/raw/branch/main/install0.sh | sh
```
