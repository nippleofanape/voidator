fastfetch
$HOME/.local/bin/quotes

#
# ZSH specific settings
#
unsetopt beep

autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit -d "$XDG_CACHE_HOME/zcompdump-$ZSH_VERSION"
_comp_options+=(globdots)
zle_highlight=('paste:none')

# vim keys
bindkey -v
export KEYTIMEOUT=1
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history
bindkey -M menuselect '^k' vi-up-line-or-history

# Change cursor shape for different vi modes.
function zle-keymap-select {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';; # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init

#
# Functions
#
n () # Remember NNN last quit directory
{
    # Block nesting of nnn in subshells
    if [[ "${NNNLVL:-0}" -ge 1 ]]; then
        echo "nnn is already running"
        return
    fi

    # The backslash allows one to alias n to nnn if desired without making an
    # infinitely recursive alias
    \nnn -Hc "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

y()
{
	tmp="$(mktemp -t "yazi-cwd.XXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

# kill processes - list only the ones you can kill. Modified the earlier script.
fkill()
{
    local pid 
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi  

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill -${15:-9}
    fi  
}

# find and edit config files
# se() {du -a $HOME/voidator/.config/* | awk '{print $2}' | fzf | xargs -r $EDITOR ;}`# with du`
se() {fd -a -c never -tf . /home/george/voidator/.config | fzf | xargs -r $EDITOR ;} #with fd

# repeat history
fh() {print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -E 's/ *[0-9]*\*? *//' | sed -E 's/\\/\\\\/g')}

#
# Aliases
#
# alias xqr="xbps-query -Rs"
# alias xq="xbps-query"
alias nv="nvim"
alias ..="cd .."
# alias b="nitrogen --set-zoom-fill --random $HOME/voidator/bg"
alias cp='advcp -g'
alias mv='advmv -g'
alias zbr='zig build run'
alias zb='zig build'
alias gaa='git add .'
alias gcm='git commit -m'
alias gp='git push'
alias gst='git status'
alias gcl='git clone'

eval "$(zoxide init zsh)"
eval "$(starship init zsh)"

#
# Plugins
#
source $XDG_DATA_HOME/zplugs/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $XDG_DATA_HOME/zplugs/zsh-autosuggestions/zsh-autosuggestions.zsh
