#!/usr/bin/env bash

PKGS=(dbus intel-ucode ntfs-3g nvidia opendoas grub-x86_64-efi \
    libX11-devel libXft-devel libXinerama-devel setxkbmap xorg-minimal xrandr xrdb xsel xsetroot xclip xdotool xdg-utils \
    dejavu-fonts-ttf font-firacode ipafont-fonts-otf nerd-fonts-otf \
    dcron curl openntpd pipewire socklog-void xtools zsh pavucontrol\
    base-devel git neovim mold cmake python3 \
    bat fzf htop pfetch ripgrep starship fd zoxide nvtop \
    nitrogen dragon dunst redshift obs ffmpegthumbnailer \
    wezterm sxiv mpv zathura-pdf-mupdf firefox flameshot telegram-desktop krita zathura-djvu
)
    # krita chromium croc nnn

REPO=https://repo-default.voidlinux.org/current
ARCH=x86_64

# Cleaning the TTY.
clear

BOLD='\e[1m'
BRED='\033[1;31m'
BBLUE='\033[1;34m'
BGREEN='\033[1;32m'
BYELLOW='\033[1;33m'
RESET='\033[0m'

# Pretty print
prompt () {
case $1 in
    "-i"|"--input")
        echo -ne "${BOLD}${BYELLOW}[ ${BGREEN}•${BYELLOW} ] $2${RESET}";;
    "-e"|"--error")
        echo -e "${BOLD}${BRED}[ ${BBLUE}•${BRED} ] $2${RESET}";;
    "-l"|"--log")
        echo -e "${BOLD}${BGREEN}[ ${BYELLOW}•${BGREEN} ] $2${RESET}";;
    *)
    echo -e "$@"
    ;;
  esac
}


# User enters a hostname.
hostname_selector () {
    prompt -i "Please enter the hostname: "
    read -r HOSTNAME
    if [[ -z "$HOSTNAME" ]]; then
        prompt -e "You need to enter a hostname in order to continue."
        return 1
    fi
    return 0
}

# Setting up a password for the user account.
userpass_selector () {
    prompt -i "Please enter name for a user account (enter empty to not create one): "
    read -r NAME
    if [[ -z "$NAME" ]]; then
        return 0
    fi
    prompt -i "Please enter a password for $NAME: "
    read -r -s userpass
    if [[ -z "$userpass" ]]; then
        echo
        prompt -e "You need to enter a password for $NAME, please try again."
        return 1
    fi
    echo
    prompt -i "Please enter the password again: " 
    read -r -s userpass2
    echo
    if [[ "$userpass" != "$userpass2" ]]; then
        echo
        prompt -e "Passwords don't match, please try again."
        return 1
    fi
    return 0
}

# Setting up a password for the root account.
rootpass_selector () {
    prompt -i "Please enter a password for the root user: "
    read -r -s ROOTPASSWD
    if [[ -z "$ROOTPASSWD" ]]; then
        echo
        prompt -e "You need to enter a password for the root user, please try again."
        return 1
    fi
    echo
    prompt -i "Please enter the password again: " 
    read -r -s rootpass2
    echo
    if [[ "$ROOTPASSWD" != "$rootpass2" ]]; then
        prompt -e "Passwords don't match, please try again."
        return 1
    fi
    return 0
}

echo -ne "${BOLD}${BYELLOW}
=======================================================================================
▀████▀   ▀███▀ ▄▄█▀▀██▄ ▀████▀███▀▀▀██▄       ██     ███▀▀██▀▀███ ▄▄█▀▀██▄ ▀███▀▀▀██▄  
  ▀██     ▄█ ▄██▀    ▀██▄ ██   ██    ▀██▄    ▄██▄    █▀   ██   ▀███▀    ▀██▄ ██   ▀██▄ 
   ██▄   ▄█  ██▀      ▀██ ██   ██     ▀██   ▄█▀██▄        ██    ██▀      ▀██ ██   ▄██  
    ██▄  █▀  ██        ██ ██   ██      ██  ▄█  ▀██        ██    ██        ██ ███████   
    ▀██ █▀   ██▄      ▄██ ██   ██     ▄██  ████████       ██    ██▄      ▄██ ██  ██▄   
     ▄██▄    ▀██▄    ▄██▀ ██   ██    ▄██▀ █▀      ██      ██    ▀██▄    ▄██▀ ██   ▀██▄ 
      ██       ▀▀████▀▀ ▄████▄████████▀ ▄███▄   ▄████▄  ▄████▄    ▀▀████▀▀ ▄████▄ ▄███▄
=======================================================================================
${RESET}"
prompt -l "Void linux installation script."

if [ "$(whoami)" != 'root' ]; then
    prompt -e "You have no permission to run $0 as non-root user."
    exit 1;
fi

prompt -l "It assumes that you have already made required partitions for a Linux filesystem and a bootloader, or installing on a working system as dual boot."

prompt -i "Format a disk and perform clean install?\n"
prompt -i "This will delete the current partition table on disk once installation starts. [y/N]?: "
read -r CLEAN_BOOT
if ! [[ "${CLEAN_BOOT,,}" =~ ^(yes|y)$ ]]; then
    CLEAN_BOOT=false
else
    CLEAN_BOOT=true
fi

# Pick a drive for a root system
lsblk -pn -o NAME,SIZE | grep nvme
prompt -i "Select a partition to install root file system\n"
PS3="[/]: "
select ROOT in $(lsblk -pnil -o NAME,TYPE | grep nvme | awk '$2 == "part" {print $1}'); do 
    prompt -l "Making a root file system on $ROOT...\n"
    mkfs.ext4 $ROOT
    mount $ROOT /mnt
    break
done

# Drive for bootloader
if [[ "$CLEAN_BOOT" == true ]]; then
    prompt -i "Select a partition to install a bootloader\n"
    PS3="[/boot]: "
    select BOOT in $(lsblk -pnil -o NAME,TYPE | grep nvme | awk '$2 == "part" {print $1}'); do 
        prompt -l "Making a bootloader file system...\n"
        mkdir -p /mnt/boot/efi
        mkfs.vfat $BOOT
        mount $BOOT /mnt/boot/efi
        break
    done
fi

prompt -l "Installing a base-system...\n"
XBPS_ARCH=$ARCH xbps-install -y -S -r /mnt -R "$REPO" base-system void-repo-nonfree

# Setting a root password
until rootpass_selector; do : ; done
echo "root:$ROOTPASSWD" | xchroot /mnt chpasswd

# Setting user password.
until userpass_selector; do : ; done
if [[ -n "$NAME" ]]; then
    prompt -l "Adding the user $NAME to the system with root privilege."
    useradd -R /mnt -G wheel,video,audio -s /usr/bin/zsh $NAME
    mkdir -p /mnt/home/$NAME/{Repos,Downloads,Desktop}
    prompt -i "Setting user password for $NAME."
    echo "$NAME:$userpass" | xchroot /mnt chpasswd
fi

# Setting a hostname
until hostname_selector; do : ; done
echo $HOSTNAME > /mnt/etc/hostname

# Copying xbps keys into chroot
mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys

prompt -l "Entering chroot..."
xchroot /mnt /bin/bash -e << EOF
    xbps-install -y -S ${PKGS[@]}

    echo -e "Editing fstab..."
    cp /proc/mounts /etc/fstab
    sed -i '/proc\|sys\|tmpfs\|pts\|LiveOS/d' /etc/fstab
    sed -i -E 's/[0-9]$/2/g' /etc/fstab
    sed -i -E '\ /\  s/[0-9]$/1/g' /etc/fstab
    echo "UUID=22148C77148C5029 /W ntfs-3g uid=${NAME},gid=${NAME},dmask=022,fmask=133 0 2" >> /etc/fstab
    echo "tmpfs	/tmp	tmpfs	defaults,nosuid,nodev	0 0" >> /etc/fstab
    mkdir /W

    ln -sf /usr/share/zoneinfo/Europe/Tallinn /etc/localtime
    git clone https://codeberg.org/nippleofanape/voidator.git /tmp/voidator
    mkdir -p /usr/share/themes
    cp -r /tmp/voidator/themes/* /usr/share/themes

    xbps-remove -RFy dash nvi sudo xfsprogs linux-firmware-amd
EOF

# Customize and install grub
if [[ "$CLEAN_BOOT" == true ]]; then
prompt -l "Installing GRUB..."
xchroot /mnt /bin/bash -e << EOF
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="Void"
    mkdir -p /usr/share/grub/themes
    cp -r /tmp/voidator/Sekiro /usr/share/grub/themes/
    sed -i '/GRUB_TIMEOUT/ s/.*/GRUB_TIMEOUT=10/' /etc/default/grub
    sed -i '/GRUB_CMDLINE_LINUX_DEFAULT/ s/.*/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=4 nvidia_drm.modeset=1"/' /etc/default/grub

    sed -i '/GRUB_THEME/d' /etc/default/grub
    echo "GRUB_THEME=\"/usr/share/grub/themes/Sekiro/theme.txt\"" >> /etc/default/grub
    sed -i '/GRUB_DISABLE_OS_PROBER/d' /etc/default/grub
    echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
EOF
fi

prompt -l "Changing fstab mountpoints to UUID's..."
mounts=$(blkid -s UUID | grep nvme)
while read -r mount uuid; do
    mount=$(echo $mount | tr -d ':')
    uuid=$(echo $uuid | tr -d '"')
    sed -i -E "s#$mount#$uuid#" /mnt/etc/fstab
done <<< "$mounts"

prompt -l "Setting up doas..."
echo -e "permit :wheel\n" > /mnt/etc/doas.conf
chown -c root:root /mnt/etc/doas.conf
chmod -c 0400 /mnt/etc/doas.conf

prompt -l "Enabling locales..."
sed -i '/en_US.UTF-8/ s/#//' /mnt/etc/default/libc-locales

prompt -l "Configuring pipewire server"
mkdir -p .mnt/etc/pipewire/pipewire.conf.d
ln -s /mnt/usr/share/examples/wireplumber/10-wireplumber.conf /mnt/etc/pipewire/pipewire.conf.d/
ln -s /mnt/usr/share/examples/pipewire/20-pipewire-pulse.conf /mnt/etc/pipewire/pipewire.conf.d/

prompt -l "Adding X11 driver settings..."
mkdir -p /mnt/etc/X11/xorg.conf.d

cat > /mnt/etc/X11/xorg.conf.d/10-nvidia-drm-outputclass.conf << EOF
Section "OutputClass"
    Identifier "intel"
    MatchDriver "i915"
    Driver "modesetting"
EndSection

Section "OutputClass"
    Identifier "nvidia"
    MatchDriver "nvidia-drm"
    Driver "nvidia"
    Option "AllowEmptyInitialConfiguration"
    Option "PrimaryGPU" "yes"
    ModulePath "/usr/lib/nvidia/xorg"
    ModulePath "/usr/lib/xorg/modules"
EndSection
EOF

cat > /mnt/etc/X11/xorg.conf.d/30-touchpad.conf << EOF
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option "NaturalScrolling" "true"
    Option "TappingButtonMap" "lrm"
    Option "AccelSpeed" "0.2"
EndSection
EOF

prompt -l "Configuring cronjobs..."
cat > /mnt/etc/cron.weekly/fstrim << EOF
#!/bin/sh

fstrim /
EOF
chmod u+x /mnt/etc/cron.weekly/fstrim

prompt -l "Adding custom scripts to /etc/rc.* ..."
echo "cat /etc/brightness > /sys/class/backlight/intel_backlight/brightness" >> /mnt/etc/rc.local
echo "cat /sys/class/backlight/intel_backlight/brightness > /etc/brightness" >> /mnt/etc/rc.shutdown
touch /mnt/etc/brightness

prompt -l "Downloading a gtk cursor theme..."
Bibata="Bibata-Modern-Pink"
curl -L https://github.com/ful1e5/Bibata_Extra_Cursor/releases/download/v1.0.1/$Bibata.tar.gz | tar -xz && mv $Bibata /mnt/usr/share/icons/

prompt -l "Reconfiguring a system before rebooting...n"
xbps-reconfigure -fa

prompt -l "Done. Don't forget to 'umount -R /mnt"

exit 0
