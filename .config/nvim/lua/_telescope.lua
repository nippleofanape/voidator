local _telescope = require('telescope')
local actions = require('telescope.actions')

_telescope.setup{
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = actions.close,
	["<C-k>"] = actions.move_selection_previous,
	["<C-j>"] = actions.move_selection_next,
      },
    },
    -- Trim the indentation at the beginning of presented line in the result window
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      "--trim" -- add this value
    },
  },
  pickers = {
    -- Remove ./ from fd results
    find_files = {
      find_command = { "fd", "--type", "f", "--strip-cwd-prefix" }
    },
  },
  extensions = {
    -- Your extension configuration goes here:
    -- extension_name = {
    --   extension_config_key = value,
    -- }
    -- please take a look at the readme of the extension you want to configure
  },
}

_telescope.load_extension('fzf')

vim.cmd.highlight({ "TelescopeBorder", "guifg=#b8bb26"})
vim.cmd.highlight({ "TelescopeNormal", "guifg=#bdae93"})
vim.cmd.highlight({ "TelescopePromptTitle", "guifg=#fb4934"})
vim.cmd.highlight({ "TelescopePromptNormal", "guifg=#fbf1c7"})
vim.cmd.highlight({ "TelescopePromptBorder", "guifg=#fab22f"})
vim.cmd.highlight({ "TelescopeSelectionCaret", "guifg=#cc241d"})
