-- Highlight on yank
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- go to last loc when opening a buffer
vim.api.nvim_create_autocmd("BufReadPost", {
  callback = function(event)
    local exclude = { "gitcommit" }
    local buf = event.buf
    if vim.tbl_contains(exclude, vim.bo[buf].filetype) or vim.b[buf].lazyvim_last_loc then
      return
    end
    vim.b[buf].lazyvim_last_loc = true
    local mark = vim.api.nvim_buf_get_mark(buf, '"')
    local lcount = vim.api.nvim_buf_line_count(buf)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(vim.api.nvim_win_set_cursor, 0, mark)
    end
  end,
})

-- Use LspAttach autocommand to only map the following keys after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
    callback = function(ev)

    local builtin = require('telescope.builtin')
    local buf = vim.lsp.buf

    -- Check wheter selected lsp supports hovering
    local client = vim.lsp.get_client_by_id(ev.data.client_id)
    if client.supports_method('textDocument/documentHighlight') then
	-- Time it takes to trigger the `CursorHold` event
	vim.o.updatetime = 1000
	-- Highlight document on hover
	local group = vim.api.nvim_create_augroup('highlight_symbol', { clear = false })
	vim.api.nvim_clear_autocmds({ buffer = ev.buf, group = group, })
	vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
	    group = group,
	    buffer = ev.buf,
	    callback = vim.lsp.buf.document_highlight,
	})
	vim.api.nvim_create_autocmd({ 'CursorMoved', 'CursorMovedI' }, {
	    group = group,
	    buffer = ev.buf,
	    callback = vim.lsp.buf.clear_references,
	})
    end

    -- Set keybindings
    if client.supports_method('textDocument/implementation') then
	vim.keymap.set('n', 'gi', buf.implementation, { noremap = true, silent = true, buffer = ev.buf, desc = 'LSP go to implementation'})
    end
    if client.supports_method('textDocument/workspace') then
	vim.keymap.set("n", "<leader>ws", builtin.lsp_workspace_symbols, { silent = true, desc = '[Tel] LSP workspace symbols'})
	vim.keymap.set('n', '<space>wa', buf.add_workspace_folder, { buffer = ev.buf, desc = 'Add a folder to workspace'})
	vim.keymap.set('n', '<space>wr', buf.remove_workspace_folder, { buffer = ev.buf, desc = 'Remove folder from workspace'})
    end
    if client.supports_method('textDocument/formatting') then
	vim.keymap.set('n', '<F2>', buf.format, { buffer = ev.buf })
    end
    -- vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Diagnostic previous'})
    -- vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Diagnostic next'})
    -- vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts )
    -- vim.keymap.set('n', '<space>D', buf.type_definition, { noremap = true, silent = true, buffer = ev.buf, desc = 'LSP go to type definition'})
    -- vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer = ev.buf, desc = 'LSP goto declaration'})
    vim.keymap.set('n', '<leader>ca', buf.code_action, { noremap = true, silent = true, buffer = ev.buf, desc = 'LSP code action'})
    vim.keymap.set('n', 'gd', builtin.lsp_definitions, { noremap = true, silent = true, buffer = ev.buf, desc = 'LSP go to definition'})
    vim.keymap.set('n', '<leader>ds', builtin.lsp_document_symbols, { noremap = true, silent = true, buffer = ev.buf, desc = '[Tel] document symbols'})
    vim.keymap.set("n", "<leader>r", builtin.lsp_references, { buffer = ev.buf, silent = true, desc = '[Tel] LSP references'})
    vim.keymap.set('n', 'gr', buf.rename, { noremap = true, silent = true, buffer = ev.buf, desc = 'LSP rename symbol'})
  end,
})
