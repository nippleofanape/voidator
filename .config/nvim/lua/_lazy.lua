local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    "nvim-lua/plenary.nvim",
    { "nvim-tree/nvim-web-devicons", lazy = true },
    { "nvim-telescope/telescope.nvim", dependencies = {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        config = function()
          require("telescope").load_extension("fzf")
        end,
      }, lazy = true, tag = '0.1.2' },
    { "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function () 
          require("nvim-treesitter.configs").setup({
              ensure_installed = { "c", "lua", "vim", "vimdoc", "cpp","rust", "toml", "bash", "make", "zig", "markdown", "wgsl" },
              sync_install = true,
              highlight = { enable = true },
            }) end
    },
    -- "nvim-treesitter/nvim-treesitter-context",
    "neovim/nvim-lspconfig",
    { "hrsh7th/nvim-cmp",
        version = false,
        event = "InsertEnter",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp-signature-help" }
    },
    { "L3MON4D3/LuaSnip", keys = function() return {} end, },
    { "mbbill/undotree", config = function() vim.g.undotree_WindowLayout = 2 end },
    { "folke/which-key.nvim", event = "VeryLazy", opts = {
	preset = "modern",
        win = {
	    no_overlap = false,
	    padding = { 1, 4 },
        }}
    },
    { "numToStr/Comment.nvim", event = "VeryLazy", config = function() require("Comment").setup() end },
    { "nvim-lualine/lualine.nvim", event = "VeryLazy", opts = { options = { theme = 'gruvbox_dark' }},
        config = function() require("lualine").setup() end
    },
    { 'echasnovski/mini.hipatterns', version = '*', config = function () 
        require('mini.hipatterns').setup({
            highlighters = {
                fixme = { pattern = '%f[%w]()FIXME()%f[%W]', group = 'lualine_a_visual' },
                todo  = { pattern = '%f[%w]()TODO()%f[%W]',  group = 'lualine_a_command'  },
                note  = { pattern = '%f[%w]()NOTE()%f[%W]',  group = 'DiagnosticInfo'  },
                hex_color = require('mini.hipatterns').gen_highlighter.hex_color(),
            },
        }) end},
    "HiPhish/rainbow-delimiters.nvim",
    "LudoPinelli/comment-box.nvim",
    { "mikavilpas/yazi.nvim", event = "VeryLazy", opts = { floating_window_scaling_factor = 0.8 } },
    { "lewis6991/gitsigns.nvim", lazy = true },
    { 'windwp/nvim-autopairs', event = "InsertEnter", opts = {} },
    { "morhetz/gruvbox", lazy = false, config = function() vim.cmd.colorscheme("gruvbox") end },
    { 'Exafunction/codeium.vim', event = 'BufEnter'},
})
