-- Shorten function name
local map = vim.keymap.set
local builtin = require('telescope.builtin')

--Remap space as leader key
map('', '<Space>', '<Nop>', { silent = true })
vim.g.mapleader = " "

-- Telescope
map("n", "<leader>f", builtin.find_files, { silent = true, desc = '[Tel] Find files'})
map("n", "<leader>ss", builtin.live_grep, { silent = true, desc = '[Tel] Live grep'})
map("n", "<leader>sb", builtin.current_buffer_fuzzy_find, { silent = true, desc = '[Tel] Search string in current buffer'})
map("n", "<leader>sc", builtin.grep_string, { silent = true, desc = '[Tel] Grep string (under cursor)'})
map("n", "<leader>j", builtin.jumplist, { silent = true, desc = '[Tel] Jumplist'})
map("n", "<leader>t", builtin.buffers, { silent = true, desc = '[Tel] Open buffers'})
map("n", "<space>b", "<Cmd>Yazi<CR>", { silent = true, desc = 'Open Yazi at the current file'})

map('n', '<leader>u', vim.cmd.UndotreeToggle, { silent = true, desc = 'Toggle undotree'})
-- Codeium
vim.g.codeium_disable_bindings = 1  -- Disable default keybindings for Codeium
map('i', '<C-CR>', function () return vim.fn['codeium#Accept']() end, { expr = true, silent = true })
map('i', '<c-o>', function() return vim.fn['codeium#CycleCompletions'](1) end, { expr = true, silent = true })
map('i', '<c-i>', function() return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true, silent = true })
map('i', '<c-x>', function() return vim.fn['codeium#Clear']() end, { expr = true, silent = true })
map('n', '<leader>cc', function() return vim.fn['codeium#Chat']() end, { expr = true, silent = true, desc = 'Codeium live chat'})

-- Navigate buffers
map("n", "<A-h>", vim.cmd.bprevious, { silent = true, desc = 'Next buffer'})
map("n", "<A-l>", vim.cmd.bnext, { silent = true, desc = 'Previous buffer'})
map("n", "<A-w>", vim.cmd.bdelete, { silent = true, desc = 'Close buffer'})

-- General
map("n", "<leader><leader>", "<cmd>Lazy<cr>", { desc = "Lazy" })
map("n", "<esc>", "<cmd>noh<cr>", { desc = "Escape and clear hlsearch"} )

map("n", "<C-h>", "<C-w>h", { desc = "Go to left window", remap = true })
map("n", "<C-l>", "<C-w>l", { desc = "Go to lower window", remap = true })
map("n", "<C-j>", "<C-w>j", { desc = "Go to bottom window", remap = true })
map("n", "<C-k>", "<C-w>k", { desc = "Go to top window", remap = true })
map("n", "<C-Right>", "<Cmd>vertical resize +5<CR>", { silent = true, desc = 'Increase window width'})
map("n", "<C-Left>", "<Cmd>vertical resize -5<CR>", { silent = true, desc = 'Decrease window width'})

map("v", "<", "<gv")
map("v", ">", ">gv")

map("n", "<A-v>", "<Cmd>vertical sb<CR>", { desc = "Vertically open buffer" })

-- Move Lines
map("n", "<A-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
map("n", "<A-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
map("i", "<A-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
map("i", "<A-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
map("v", "<A-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
map("v", "<A-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })

-- Registers
map({'n', 'v'}, "<leader>y", "\"+y", { silent = true, desc = 'Yank to clipboard'})
map({'n', 'v'}, "<leader>d", "\"_d", { silent = true, desc = 'Delete to void'})
map("n", "x", "\"_x", { silent = true })
map("n", "'", "`", { silent = true })
