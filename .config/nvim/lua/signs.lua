require('gitsigns').setup{
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    -- Navigation
    map('n', ']c', function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, {expr=true, desc = 'Git next hunk'})

    map('n', '[c', function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, {expr=true, desc = 'Git previous hunk'})

    -- Actions
    map('n', '<leader>gs', gs.stage_hunk, { desc = 'Git stage hunk' })
    map('n', '<leader>gr', gs.reset_hunk, { desc = 'Git reset hunk' })
    map('n', '<leader>gu', gs.undo_stage_hunk, { desc = 'Git undo stage hunk' })
    map('n', '<leader>gS', gs.stage_buffer, { desc = 'Git stage buffer' })
    map('n', '<leader>gR', gs.reset_buffer, { desc = 'Git reset buffer' })
    map('n', '<leader>gv', gs.preview_hunk, { desc = 'Git preview hunk' })
    map('n', '<leader>gd', gs.diffthis, { desc = 'Git diff (short)' })
    map('n', '<leader>gD', function() gs.diffthis('~') end, { desc = 'Git diff' })
    map('n', '<leader>gt', gs.toggle_deleted, { desc = 'Git toggle deleted' })

    -- Text object
    map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
  end
}
