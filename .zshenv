path+=('/home/george/.local/bin' '/home/george/Repos/zigup/zig-out/bin')
fpath+=$HOME/.local/share/zfunc
export HISTSIZE=5000
export SAVEHIST=5000
export HIST_STAMPS="dd-mm-yyyy"
export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export HISTFILE="${XDG_CACHE_HOME}/zsh_history"
export ZSH_COMPDUMP="${XDG_CACHE_HOME}/zcompdump-${ZSH_VERSION}"
export EDITOR=nvim
export MANPAGER="$HOME/.local/bin/batman"
export BAT_THEME="Dracula"
export NNN_BMS="h:$HOME/;w:/W/;z:/W/DownloadZ/;c:$HOME/.config/;b:$HOME/.local/bin/;r:$HOME/Repos/;t:$HOME/Desktop/;d:$HOME/Downloads/;v:$HOME/voidator"
export NNN_FIFO=/tmp/nnn.fifo
export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
export NNN_PLUG='d:dragdrop;e:suedit;j:autojump;i:imgview'
#export NNN_OPENER=$HOME/.config/nnn/plugins/nuke
export NNN_COLORS='#c7d0512f'
export NNN_FCOLORS='c1e2d12ed360abf7c6d6abc4'
export PF_INFO="ascii kernel os wm shell uptime pkgs memory"
export PF_SEP=" >"
export PF_ALIGN=8
export PF_COL2=8
export CM_HISTLENGTH=10
export FZF_DEFAULT_OPTS='--color=bg+:#3c3836,bg:#32302f,spinner:#fb4934,hl:#928374,fg:#ebdbb2,header:#928374,info:#8ec07c,pointer:#fb4934,marker:#fb4934,fg+:#ebdbb2,prompt:#fb4934,hl+:#fb4934 --height 50% --border --layout=reverse --margin=1 --padding=1'
. "$HOME/.cargo/env"
